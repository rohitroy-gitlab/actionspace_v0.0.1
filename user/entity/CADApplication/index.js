import {CADApplicationClass} from "../../plugin/CADApplication.js";

const GetCADApplication = {
  scope: "Browser",
  name: "CADApplication",
  path: "^#GetCADApplication$",
  entity: "Ehh",
  model: {
    rectangles: [],
  },
  view(model) {
    var container = document.getElementById("ehh");
    container.innerHTML = `
    <div class="mainContainer">
    <div id="drawingCanvas"></div>
    <div id="sidebar">

      <div class="functionButtons">
        <div class="col">
          <button id="drawBtn" class="funcBtn">Draw</button>
          <button id="resetBtn" class="funcBtn">Reset Canvas</button>
        </div>
        <div class="col">
          <button id="deleteBtn" class="funcBtn">Delete Element</button>
          <button id="dragBtn" class="funcBtn">Drag Element</button>
        </div>  
      </div>

      <div class="htmlElementMenu">
        <div class="col">
          <button id="addDiv" class="elementBtn">Div</button>
          <button id="addHeading" class="elementBtn">Heading [h3]</button>
          <button id="addParagraph" class="elementBtn">Paragraph</button>
        </div>
        <div class="col">
          <button id="addImage" class="elementBtn">Image</button>
          <button id="addTable" class="elementBtn">Table</button>
          <button id="addForm" class="elementBtn">Form</button>
        </div>
      </div>

      <div class="rectangleData">
        <h4>Rectangle Data</h4>
        <ul id="rectList"></ul>
      </div>
    </div>
  </div>`;
    // let CADApplicationClassAB = new CADApplicationClass(model.rectangles);
    let CADApplicationClassAB = new CADApplicationClass();
  },
  controller: {
    name: "CADApplicationPage",
    callback: async (event, view, model, callback) => {
      view(model);

      // // addTheClass
      // let AudioPlayerAB = new AudioPlayer(model.songs);
      // AudioPlayerAB.loadSong(model.songs);
      // // loadSong(songs[songIndex]);
      // await callback(event, view, model, callback);
    },
  },
};
const CADApplication = {
  GetCADApplication,
};
export {CADApplication};
