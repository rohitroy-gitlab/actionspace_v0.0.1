class PlaylistAudioPlayerClass {
  constructor(audioFiles) {
    this.audioFiles = audioFiles;
    this.currentTrack = 0;

    this.fileInput = document.getElementById("fileInput");
    this.playBtn = document.getElementById("playBtn");
    this.pauseBtn = document.getElementById("pauseBtn");
    this.prevBtn = document.getElementById("prevBtn");
    this.nextBtn = document.getElementById("nextBtn");
    this.audioElement = document.getElementById("audioElement");
    this.resetBtn = document.getElementById("resetBtn");
    this.songDisplay = document.getElementById("songDisplay");

    this.eventListener();
  }

  eventListener() {
    this.fileInput.addEventListener("change", (e) => {
      this.audioFiles = Array.from(e.target.files);
      this.songDisplay.innerHTML = this.audioFiles[this.currentTrack].name;
      this.loadAudio(this.currentTrack);
    });

    this.playBtn.addEventListener("click", () => {
      this.audioElement.play();
    });

    this.pauseBtn.addEventListener("click", () => {
      this.audioElement.pause();
    });

    this.prevBtn.addEventListener("click", this.playPreviousSong.bind(this));

    this.nextBtn.addEventListener("click", this.playNextSong.bind(this));

    this.resetBtn.addEventListener("click", this.resetSelection.bind(this));
  }

  loadAudio(index) {
    this.audioElement.src = URL.createObjectURL(this.audioFiles[index]);
    this.audioElement.load();

    this.songDisplay.innerHTML = this.audioFiles[this.currentTrack].name;
    // this.audioElement.play();
  }

  resetSelection() {
    this.audioElement.pause();
    this.audioFiles.length = 0;
    this.fileInput.value = "";
  }

  playNextSong() {
    if (this.currentTrack === this.audioFiles.length - 1) {
      this.currentTrack = 0;
    } else {
      this.currentTrack++;
    }
    this.loadAudio(this.currentTrack);
  }

  playPreviousSong() {
    if (this.currentTrack === 0) {
      this.currentTrack = this.audioFiles.length - 1;
    } else {
      this.currentTrack--;
    }
    this.loadAudio(this.currentTrack);
  }
}

export {PlaylistAudioPlayerClass};
