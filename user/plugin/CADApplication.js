// CADApplicationClass
// rohitroy

class CADApplicationClass {
  constructor() {
    this.resetBtn = document.querySelector("#resetBtn");
    this.deleteBtn = document.querySelector("#deleteBtn");

    this.drawBtn = document.querySelector("#drawBtn");
    // settingDefaultdrawBtnToRed
    this.drawBtn.style.backgroundColor = "red";

    this.dragBtn = document.querySelector("#dragBtn");
    // settingDefaultdragBtnToRed
    this.dragBtn.style.backgroundColor = "red";
    this.selectedChild;
    this.isDragging = false;
    this.mouseOffset = {x: 0, y: 0};
    this.allowedToDrag = false;
    this.children;
    this.i;

    // forDragModule2
    this.initialXDrag;
    this.initialYDrag;
    this.xOffsetDrag = 0;
    this.yOffsetDrag = 0;

    // forDrawnRectangleData
    this.rectList = document.querySelector("#rectList");
    this.drawingCanvas = document.querySelector("#drawingCanvas");

    this.isDrawing = false;
    this.startX = 0.0;
    this.startY = 0.0;
    this.currentRect;
    this.rectItem;

    this.rectIdToBeDeleted;
    this.rectangleToBeDelete;

    // switchForDrawButton
    this.allowedToDraw = false;

    // fetchingDrawingCanvasHeight
    // this.drawingCanvasHeight = this.drawingCanvas.offsetHeight;

    // topOffsetFor(e.clientY)Calculation = navbarHeight + blackBorderHeight
    this.topOffset = 53;
    // leftOffsetFor(e.clientX)Calculation = blackBorderHeight
    this.leftOffset = 3;

    // contructorsForNewlyCreatedHTMLElements
    this.addDiv = document.querySelector("#addDiv");
    this.addHeading = document.querySelector("#addHeading");
    this.addParagraph = document.querySelector("#addParagraph");
    this.addImage = document.querySelector("#addImage");
    this.addTable = document.querySelector("#addTable");
    this.addForm = document.querySelector("#addForm");

    this.eventListener();
  }

  // navbarHeight + BlackBorder = 53px

  // alleventListenersDefinedHere
  eventListener() {
    // resetButtoneventListener
    this.resetBtn.addEventListener("click", () => {
      // check
      // console.log(this.drawingCanvasHeight);
      // console.log("reset-fired");

      this.drawBtn.style.backgroundColor = "red";
      this.allowedToDraw = false;
      this.isDrawing = false;

      // clearingRectangleDataList
      this.rectList.innerHTML = "";
      // clearingDrawingCanvas
      this.drawingCanvas.innerHTML = "";
    });

    //deleteButtonEventListener
    this.deleteBtn.addEventListener("click", () => {
      // removeTheSelectedDivElementFromCanvas;
      this.drawingCanvas.removeChild(this.rectangleToBeDelete);

      // fetchingIDOfTheSelectedDivElementToDelete
      const matchingAttributeElement = this.rectList.querySelector(
        `li[data-rect-id="${this.rectIdToBeDeleted}"]`
      );

      // removingSpecificIdListElementFromRectangleList
      rectList.removeChild(matchingAttributeElement);

      // check
      // console.log("deleted : ", rectIdToBeDeleted);
    });

    // drawButtonEventListener
    this.drawBtn.addEventListener("click", () => {
      // checkIfAlreadyClickedBefore?
      if (this.allowedToDraw) {
        // ifGreen
        this.drawBtn.style.backgroundColor = "red";
        this.allowedToDraw = false;
        this.isDrawing = false;
      } else {
        // IfRed
        this.drawBtn.style.backgroundColor = "green";
        this.allowedToDraw = true;
        this.isDrawing = true;
        this.allowedToDrag = false;
        this.dragBtn.style.backgroundColor = "red";
      }
    });

    // rectListEventListener(selectRectanglesOnSideBar)
    this.rectList.addEventListener("click", (e) => {
      const selectedRect = document.querySelector(".selected");

      if (selectedRect) {
        selectedRect.classList.remove("selected");
      }

      const rectId = e.target.dataset.rectId;

      // selectingSameRectangleIDToBeDeleted?
      this.rectIdToBeDeleted = e.target.dataset.rectId;

      // ifRectSelected?
      if (rectId) {
        const rect = document.querySelector(`[data-rect="${rectId}"]`);

        // selectingSameRectangleToBeDeleted?
        this.rectangleToBeDelete = document.querySelector(
          `[data-rect="${rectId}"]`
        );

        rect.classList.add("selected");
      }
    });

    this.drawingCanvas.addEventListener("mousedown", (e) => {
      if (this.isDrawing && this.allowedToDraw) {
        this.startX = e.clientX - this.leftOffset;
        this.startY = e.clientY - this.topOffset;
        this.currentRect = document.createElement("div");
        this.currentRect.classList.add("rect");
        this.currentRect.style.left = this.startX + "px";
        this.currentRect.style.top = this.startY + "px";

        // this.currentRect.style.top =
        //   this.startY + (e.clientY - this.drawingCanvasHeight) + "px";

        // document.body.appendChild(currentRect);
        this.drawingCanvas.appendChild(this.currentRect);
      }
    });

    this.drawingCanvas.addEventListener("mousemove", (e) => {
      if (this.currentRect && this.isDrawing && this.allowedToDraw) {
        const deltaX = e.clientX - this.startX - 3;
        const deltaY = e.clientY - this.startY - 53;

        this.currentRect.style.width = Math.abs(deltaX) + "px";
        this.currentRect.style.height = Math.abs(deltaY) + "px";

        // this.currentRect.style.width = deltaX + "px";
        // this.currentRect.style.height = deltaY + "px";

        // check
        // console.log("deltaX" + deltaX);
        // console.log("deltaY" + deltaY);

        // forCases=(top-left | top-right) movements
        if (deltaX < 0) {
          this.currentRect.style.left = e.clientX - this.leftOffset + "px";
        }
        if (deltaY < 0) {
          this.currentRect.style.top = e.clientY - this.topOffset + "px";

          // this.currentRect.style.top =
          //   e.clientY + (e.clientY - this.drawingCanvasHeight) + "px";
        }
      }
    });

    this.drawingCanvas.addEventListener("mouseup", (e) => {
      if (this.currentRect && this.allowedToDraw) {
        // stopDrawing

        // fetchingDivDimentions
        const rect = {
          x: parseInt(this.currentRect.style.left),
          y: parseInt(this.currentRect.style.top),
          width: parseInt(this.currentRect.style.width),
          height: parseInt(this.currentRect.style.height),
        };
        // printingDivDetails
        const rectId = Date.now();
        this.currentRect.setAttribute("data-rect", rectId);
        this.rectItem = document.createElement("li");
        this.rectItem.innerHTML = `> Rectangle (x1 : ${rect.x}, y1 : ${
          rect.y
        }) - (x2 : ${rect.x + rect.width}, y2 : ${rect.y + rect.height})`;
        this.rectItem.setAttribute("data-rect-id", rectId);
        this.rectList.appendChild(this.rectItem);
        this.currentRect = null;
      }
    });

    // ###############################
    // draggingModuleStarts
    // ###############################

    // draggingModule1

    // // onClickingDragBtn
    // this.dragBtn.addEventListener("click", () => {
    //   this.children = document.getElementsByClassName("rect");
    //   if (this.allowedToDrag) {
    //     console.log("Stop Dragging !");
    //     this.allowedToDrag = false;
    //     this.dragBtn.style.backgroundColor = "red";

    //     // enablingDrawingFunctions
    //     this.drawBtn.style.backgroundColor = "green";
    //     this.allowedToDraw = true;
    //     this.isDrawing = true;
    //   } else {
    //     console.log("Start Dragging !");
    //     console.log(this.children);
    //     this.allowedToDrag = true;
    //     this.dragBtn.style.backgroundColor = "green";

    //     // stoppingDrawingFunctions
    //     this.allowedToDraw = false;
    //     this.drawBtn.style.backgroundColor = "red";
    //   }

    //   for (let i = 0; i < this.children.length; i++) {
    //     console.log(this.children[i]);
    //     this.children[i].addEventListener("mousedown", (event) => {
    //       console.log("mousedown");
    //       console.log(this.children[i]);
    //       if (this.allowedToDrag) {
    //         this.selectedChild = event.target;
    //         this.selectedChild.classList.add("draggableElement");
    //         // this.selectedChild.addC;
    //         console.log(this.selectedChild);
    //         this.isDragging = true;
    //         this.mouseOffset.x = event.offsetX;
    //         this.mouseOffset.y = event.offsetY;
    //       }
    //     });
    //   }

    //   document.addEventListener("mousemove", (event) => {
    //     // console.log("mousemove");
    //     if (this.isDragging && this.selectedChild) {
    //       let newX =
    //         event.pageX - this.mouseOffset.x - this.drawingCanvas.offsetLeft;
    //       let newY =
    //         event.pageY - this.mouseOffset.y - this.drawingCanvas.offsetTop;
    //       this.selectedChild.style.left = newX - this.leftOffset + "px";
    //       this.selectedChild.style.top = newY - this.topOffset + "px";
    //     }
    //   });

    //   document.addEventListener("mouseup", (event) => {
    //     console.log("mouseup");

    //     this.isDragging = false;
    //     this.selectedChild.classList.remove("draggableElement");
    //   });
    // });

    // draggingModule2

    // onClickingDragBtn
    this.dragBtn.addEventListener("click", () => {
      this.children = document.getElementsByClassName("rect");
      if (this.allowedToDrag) {
        console.log("Stop Dragging !");
        this.allowedToDrag = false;
        this.dragBtn.style.backgroundColor = "red";

        // enablingDrawingFunctions
        this.drawBtn.style.backgroundColor = "green";
        this.allowedToDraw = true;
        this.isDrawing = true;
      } else {
        console.log("Start Dragging !");
        console.log(this.children);
        this.allowedToDrag = true;
        this.dragBtn.style.backgroundColor = "green";

        // stoppingDrawingFunctions
        this.allowedToDraw = false;
        this.drawBtn.style.backgroundColor = "red";
      }

      for (let i = 0; i < this.children.length; i++) {
        console.log(this.children[i]);
        this.children[i].addEventListener("mousedown", (event) => {
          console.log("mousedown");
          console.log(this.children[i]);
          if (this.allowedToDrag) {
            this.selectedChild = event.target;
            this.selectedChild.classList.add("draggableElement");
            console.log(this.selectedChild);
            this.isDragging = true;
            this.initialXDrag = event.clientX - this.xOffsetDrag;
            this.initialYDrag = event.clientY - this.yOffsetDrag;
          }
        });
      }

      document.addEventListener("mousemove", (event) => {
        // console.log("mousemove");
        if (this.isDragging) {
          // event.preventDefault();

          let currentX = event.clientX - this.initialXDrag;
          let currentY = event.clientY - this.initialYDrag;

          this.xOffsetDrag = currentX;
          this.yOffsetDrag = currentY;

          this.selectedChild.style.transform =
            "translate(" + currentX + "px, " + currentY + "px)";

          // Snap the child to the nearest grid line
          let gridSize = 50; // adjust as needed
          let xSnap = this.getNearestGridLine(currentX, gridSize);
          let ySnap = this.getNearestGridLine(currentY, gridSize);

          this.selectedChild.style.transform =
            "translate(" + xSnap + "px, " + ySnap + "px)";
        }
      });

      document.addEventListener("mouseup", (event) => {
        console.log("mouseup");

        this.isDragging = false;

        this.initialXDrag = this.xOffsetDrag;
        this.initialYDrag = this.yOffsetDrag;

        // Snap the child to the nearest grid intersection
        let gridSize = 50; // adjust as needed
        let xSnap =
          Math.round(this.selectedChild.offsetLeft / gridSize) * gridSize;
        let ySnap =
          Math.round(this.selectedChild.offsetTop / gridSize) * gridSize;

        this.selectedChild.style.left = xSnap + "px";
        this.selectedChild.style.top = ySnap + "px";

        // child.style.transform = `translate(${xSnap}px, ${ySnap}px)`;

        this.selectedChild.classList.remove("draggableElement");
      });
    });

    // ###############################
    // draggingModuleEnds
    // ###############################

    // ###############################
    // click&AddHTMLElementsModuleStarts
    // ###############################

    // addingANewDivElementOnButttonPress
    this.addDiv.addEventListener("click", () => {
      const newDiv = document.createElement("div");
      newDiv.classList.add("newlyCreatedDivElement"); // add class to new div

      // adddingDraggableClasses
      newDiv.classList.add("rect");

      this.drawingCanvas.appendChild(newDiv);
    });

    // addingANewHeadingElementOnButttonPress;
    this.addHeading.addEventListener("click", () => {
      const newHeading = document.createElement("h3");
      newHeading.classList.add("newlyCreatedHeadingElement");
      newHeading.setAttribute("contenteditable", true);
      newHeading.textContent = "Click to edit <h3> content";

      // adddingDraggableClasses
      newHeading.classList.add("rect");

      this.drawingCanvas.appendChild(newHeading);
    });

    // addingANewParagraphElementOnButttonPress
    this.addParagraph.addEventListener("click", () => {
      const newParagraph = document.createElement("p");
      newParagraph.classList.add("newlyCreatedParagraphElement"); // add class to new paragraph
      newParagraph.setAttribute("contenteditable", true);
      newParagraph.textContent = "Click to edit <p>";

      // adddingDraggableClasses
      newParagraph.classList.add("rect");

      this.drawingCanvas.appendChild(newParagraph);
    });

    // addingANewImageElementOnButttonPress
    this.addImage.addEventListener("click", () => {
      const img = document.createElement("img");
      img.src = "./user/entity/CADApplication/demoImage.jpg";
      img.alt = "demoImage.jpg";
      img.classList.add("newlyCreatedImageElement");

      // adddingDraggableClasses
      img.classList.add("rect");

      this.drawingCanvas.appendChild(img);
    });

    //addingANewTableElementOnButttonPress
    this.addTable.addEventListener("click", () => {
      const rows = parseInt(prompt("How many rows ?"));
      const cols = parseInt(prompt("How many columns ?"));

      var table = document.createElement("table");
      table.classList.add("newlyCreatedFormElement");
      // var rows = 3; // Set the number of rows
      // var cols = 3; // Set the number of columns
      for (var i = 0; i < rows; i++) {
        var row = table.insertRow(i);
        for (var j = 0; j < cols; j++) {
          var cell = row.insertCell(j);
          cell.innerHTML = "Row " + i + ", Column " + j;
        }
      }

      // adddingDraggableClasses
      table.classList.add("rect");

      this.drawingCanvas.appendChild(table);
    });

    // addingANewFormElementOnButttonPress
    this.addForm.addEventListener("click", () => {
      const numInputs = parseInt(prompt("Enter number of inputs:"));
      const numButtons = parseInt(prompt("Enter number of buttons:"));

      // Create a new form element
      const form = document.createElement("form");
      form.classList.add("newlyCreatedFormElement");

      for (let i = 0; i < numInputs; i++) {
        const label = document.createElement("label");
        label.textContent = `Input ${i + 1}: `;
        const input = document.createElement("input");
        input.type = "text";
        label.appendChild(input);
        form.appendChild(label);
      }
      for (let i = 0; i < numButtons; i++) {
        const button = document.createElement("button");
        button.textContent = `Button ${i + 1}`;
        form.appendChild(button);
      }

      // adddingDraggableClasses
      form.classList.add("rect");

      this.drawingCanvas.appendChild(form);
    });

    // ###############################
    // click&AddHTMLElementsModuleStarts
    // ###############################
  }

  // functionToGetNearestGridLine
  getNearestGridLine(pos, gridSize) {
    // Calculate the nearest grid line to the current position
    let snapTo = Math.round(pos / gridSize) * gridSize;
    let distanceToSnapTo = Math.abs(pos - snapTo);
    let distanceToNext = Math.abs(pos - (snapTo + gridSize));
    let distanceToPrev = Math.abs(pos - (snapTo - gridSize));

    if (
      distanceToSnapTo <= distanceToNext &&
      distanceToSnapTo <= distanceToPrev
    ) {
      return snapTo;
    } else if (distanceToNext <= distanceToPrev) {
      return snapTo + gridSize;
    } else {
      return snapTo - gridSize;
    }
  }
}

export {CADApplicationClass};
