const browserConfig = {
  name: "EhhStart",
  env: "Browser",
  Port: "4000",
  host: window.location.origin,
  Entity: {
    Ehh: "/user/entity/Ehh/index.js",
    Github: "/user/entity/Github/index.js",
    AudioPlayer: "/user/entity/AudioPlayer/index.js",
    PlaylistAudioPlayer: "/user/entity/PlaylistAudioPlayer/index.js",
    CADApplication: "/user/entity/CADApplication/index.js",
  },
  homePage: "#Home",
};

export {browserConfig};
